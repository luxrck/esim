import torch
from torch import nn, optim
import torch.nn.functional as F

from tqdm import tqdm

from dataset import embedding_weight, SnliDataset

import sys
import os
import re

class SnliNN(nn.Module):
    def __init__(self, num_embeddings, embedding_dim, hidden_dims, dropout_p=0.5):
        super(SnliNN, self).__init__()
        self.emb = nn.Embedding(num_embeddings, embedding_dim)
        self.emb.weight = nn.Parameter(embedding_weight)
        net = []
        for l,m in zip(hidden_dims[:-1], hidden_dims[1:]):
            net.append(nn.Linear(l,m))
            #net[-1].weight = nn.Parameter(torch.randn(m,l) / torch.sqrt(torch.tensor(2.0)/l))
            net.append(nn.Dropout(p=dropout_p))
            net.append(nn.ReLU())
        net.pop(-1)
        self.nn = nn.Sequential(*net)
    def sent2vec(self, s):
        return torch.sum(s, dim=0) / s.size(0)
    def embedding(self, x):
        batch_size = len(x)
        _x = []
        for s in x:
            s0, s1 = s
            s0 = torch.tensor(s0).cuda()
            s1 = torch.tensor(s1).cuda()
            s0 = self.emb(s0)
            s1 = self.emb(s1)
            s0 = self.sent2vec(s0)
            s1 = self.sent2vec(s1)
            s = torch.cat([s0, s1])
            s = s.view(1, *s.size())
            _x.append(s)
        return torch.cat(_x)
    def forward(self, x):
        if torch.cuda.is_available():
            x = x.cuda()
        x = self.nn(x)
        return x


class ESIM(nn.Module):
    def __init__(self, embedding_weight, out_dim, num_embeddings, embedding_dim, hidden_dim, dropout_p=0.0):
        super(ESIM, self).__init__()
        self.emb = nn.Embedding(num_embeddings, embedding_dim)
        self.emb.weight = nn.Parameter(embedding_weight)
        self.input_encoding = nn.LSTM(input_size=embedding_dim, hidden_size=embedding_dim, batch_first=True, dropout=dropout_p, bidirectional=True)
        self.nn = nn.Sequential(
                    nn.Linear(4 * 2 * embedding_dim, 4 * 2 * embedding_dim),
                    nn.Dropout(p=dropout_p),
                    nn.ReLU(),
                    nn.Linear(4 * 2 * embedding_dim, embedding_dim),
                    nn.Dropout(p=dropout_p),
                    nn.ReLU(),
                    nn.Linear(embedding_dim, out_dim))

    # input shape: (batch, sentence_len)
    # 我的实现是预先将句子padding到统一的长度以方便训练
    def forward(self, inputs):
        # import pdb; pdb.set_trace()
        input_p, input_h = inputs[0], inputs[1]

        batch_size =len(inputs[0])

        # Input Encoding
        xp = self.emb(input_p)
        xh = self.emb(input_h)

        # Sentence embedding
        op, _ = self.input_encoding(xp) # (batch, sentence_len, embedding_dim)
        oh, _ = self.input_encoding(xh)

        # Local Inference Modeling (with soft-align attention)
        E_ph = torch.matmul(op, oh.transpose(-1, -2)) # We calculate the similarity matrix of op and oh
        wp = torch.matmul(F.softmax(E_ph, dim=2), oh)
        wh = torch.matmul(F.softmax(E_ph, dim=1).transpose(-1, -2), op)

        # Enhancement of local inference information
        mp = torch.cat([op, wp, op-wp, op*wp], dim=1)
        mh = torch.cat([oh, wh, oh-wh, oh*wh], dim=1)

        # Pooling
        embedding_dim = mp.size(-1)
        vp_a = F.avg_pool1d(mp.transpose(-1,-2), embedding_dim).view(batch_size, -1)
        vp_m = F.max_pool1d(mp.transpose(-1,-2), embedding_dim).view(batch_size, -1)
        vh_a = F.avg_pool1d(mh.transpose(-1,-2), embedding_dim).view(batch_size, -1)
        vh_m = F.max_pool1d(mh.transpose(-1,-2), embedding_dim).view(batch_size, -1)

        v = torch.cat([vp_a, vp_m, vh_a, vh_m], dim=1)

        y_p = self.nn(v)
        return y_p



# We will exec `fn` immediately instead of return a wrapper function
def train(model, criterion, optimizer, dataloader, epochs, device=None, half=False, checkpoint=None, validation=None, immediate=False, verbose=False, fn=None):
    start = 0
    model_name = model.__class__.__name__

    # %%
    # 让model这么早就映射到`device`的原因是:
    # 如果我们打开`checkpoint`开关, 对于一些优化器, 我们需要加载它们之前被保存的状态,
    # 这就带来了一个问题: Optimizer::load_state_dict的实现(torch v1.1.0)是自动将state
    # 映射到`model.parameters()`所在的`device`上面, 并且将state的dtype设置成和模型参数一样的dtype.
    # 如果我们不先把model映射好, load_state之后如果我们要改变model的device的话, 会造成optimizer
    # 的参数位置和model的不在同一设备上, 即出错.
    # 所以我们必须要一开始就先把model给映射好. 并且将模型的参数类型也要设置好(比如是否用half)
    # %%
    # 那么问题来了, model.load_state_dict会不会改变model的device呢?
    # 答: 不会. model.load_state_dict加载参数最终调用的是Tensor::copy_, 这并不会改变Tensor的device.
    # 所以我们可以放心地在一开始就映射好model
    model = model.to(device)
    criterion = criterion.to(device)
    if half:
        model, criterion = model.half(), criterion.half()

    if not device:
        device = torch.device("cuda" if torch.cuda.is_available() else "cpu")
    if checkpoint:
        if checkpoint == True:
            checkpoint = "./checkpoint"
        if not os.path.exists(checkpoint):
            os.mkdir(checkpoint)
        state = None
        key = lambda x: re.search(f"(?<={model_name}\.)(\d+)(?=\.torch)", x).group(0)
        fastforward = list(filter(key, list(os.walk(checkpoint))[0][2]))
        if fastforward:
            fastforward = max(fastforward, key=lambda x: int(key(x)))
            fastforward = os.path.join(checkpoint, fastforward)
            state = torch.load(fastforward, map_location=device)
        if state:
            start = state["start"]
            model.load_state_dict(state["model"])
            # By default all the modules are initialized to train mode (self.training = True).
            # Also be aware that some layers have different behavior during train/and evaluation
            # (like BatchNorm, Dropout) so setting it matters.
            model.train()
            optimizer.load_state_dict(state["optim"])

    def to_device(inputs, labels, device):
        if isinstance(inputs, list) or isinstance(inputs, tuple):
            inputs = torch.cat([i.view(1, *i.size()) for i in inputs], dim=0).to(device)
        inputs = inputs.to(device)
        labels = labels.to(device)
        if half and inputs.is_floating_point():
            inputs = inputs.half()
        return inputs, labels

    def _validate(fn, dataloader):
        correct, total = 0, 0
        with torch.no_grad():
            for i,(inputs,labels) in tqdm(enumerate(dataloader)):
                inputs, labels = to_device(inputs, labels, device)
                y_predict, _ = fn(model, criterion, inputs, labels)
                _, l_predict = torch.max(y_predict, dim=1)
                correct_cnt = (l_predict == labels).sum()
                total_cnt = labels.size(0)
                correct += correct_cnt.item()
                total += total_cnt
        return correct, total

    def _train(fn):
        for epoch in range(start, epochs):
            total_loss = 0
            for i,(inputs,labels) in tqdm(enumerate(dataloader)):
                model.zero_grad()
                inputs, labels = to_device(inputs, labels, device)

                _, loss = fn(model, criterion, inputs, labels)

                loss.backward()
                optimizer.step()
                #import pdb; pdb.set_trace()

                total_loss += loss.item() # / inputs.size(0)
                if verbose:
                    if i % 100 == 0:
                        print(f"epoch: {epoch}, iter: {i}, loss: {loss}, total_loss: {total_loss}")

            if checkpoint:
                torch.save({"start": epoch + 1, "model": model.state_dict(), "optim": optimizer.state_dict()},
                            os.path.join(checkpoint, f"{model.__class__.__name__}.{epoch}.torch"))

            correct, total = 0, 0
            printed_str = f"epoch: {epoch}, loss: {total_loss}"
            if validation:
                correct, total = _validate(fn, validation)
                printed_str += " " + f"validate: {total}, {correct/total}"
            print(printed_str)
        return model

    def _train_wrapper(fn):
        if immediate:
            return _train(fn)
        def _train_deco():
            return _train(fn)
        return _train_deco

    if fn: return _train(fn)
    return _train_wrapper



# TODO: 当batch_size取到16的时候会出现 "NNPACK SpatialConvolution_updateOutput failed", NMD! 为什么?
if __name__ == "__main__":
    lr = 0.0004
    batch_size = 32
    epochs = 5
    # 我用glove.200d
    embedding_dim = 200
    nn_hidden_dims = (400, 800, 3)
    hidden_dim = 200
    max_padding_sentence_len = 26
    dropout_p = 0.5
    out_dim = 3
    # 如果使用半精度训练的话, 默认的eps为1e-8, 在半精度下会被round到0.
    # 对于Adam而言, 就会造成除零错(nan), 所以要把eps设高些.
    adam_eps = 1e-4
    device = torch.device("cuda" if torch.cuda.is_available() else "cpu")

    def sent_collate_fn(batch):
        p, h, labels = [], [], []
        for data in batch:
            p.append(data[0][0])
            h.append(data[0][1])
        #    inputs.append((data[0], data[1]))
            labels.append(data[1])
        # import pdb; pdb.set_trace()
        return ((torch.tensor(p), torch.tensor(h)), torch.tensor(labels))

    data = SnliDataset(dtype="train")
    test_data = SnliDataset(dtype="test")

    # net = SnliNN(data.dictionary_size, embedding_dim, nn_hidden_dims, dropout_p=dropout_p)
    net = ESIM(embedding_weight=embedding_weight, out_dim=out_dim, num_embeddings=data.dictionary_size, embedding_dim=embedding_dim, hidden_dim=hidden_dim)
    criterion = nn.CrossEntropyLoss()
    optimizer = optim.Adam(net.parameters(), lr=lr, eps=1e-4)
    # optimizer = optim.SGD(net.parameters(), lr=lr, momentum=0.8)

    train_loader = torch.utils.data.DataLoader(dataset=data.train(), batch_size=batch_size, shuffle=True, collate_fn=sent_collate_fn)
    test_loader = torch.utils.data.DataLoader(dataset=test_data, batch_size=1, shuffle=False, collate_fn=sent_collate_fn)
    @train(net, criterion, optimizer, train_loader, epochs, validation=test_loader, device=device, half=True, immediate=True, checkpoint=True, verbose=False)
    def snli_train(model, criterion, inputs, labels):
        # inputs = torch.cat([i.view(1, *i.size()).to(device) for i in inputs], dim=0)
        y_predict = model(inputs)
        loss = criterion(y_predict, labels)
        return y_predict, loss

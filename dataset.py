from tqdm import tqdm

import torch
from torch.utils.data import Dataset
import torch.nn.functional as F

from torchvision import transforms

import numpy as np
import pickle

_config = pickle.load(open("data/preprocessing.pkl", "rb"))

embedding_weight = torch.tensor(_config["word_vec"])
# 我们选取0代表空的Embedding向量
class SnliDataset(Dataset):
    def __init__(self, max_padding_sentence_len=-1, dtype="train"):
        # self.word_vec = config["word_vec"]
        self.sentences_train = _config["sentences_train"]
        self.sentences_test = _config["sentences_test"]
        self.sentences_dev = _config["sentences_dev"]
        self.bow_train = _config["bow_train"]
        self.bow_test = _config["bow_train"]
        self.bow_dev = _config["bow_train"]
        #self.sentences_vec_train = _config["sentences_vec_train"]
        #self.sentences_vec_test = _config["sentences_vec_test"]
        #self.sentences_vec_dev = _config["sentences_vec_dev"]
        self.dictionary = _config["dictionary"]
        self.dictionary_size = len(self.dictionary) + 1
        self.suggested_padding_sentence_len = _config["suggested_padding_sentence_len"]
        self.max_padding_sentence_len = _config["max_padding_sentence_len"]
        if max_padding_sentence_len != -1:
            self.max_padding_sentence_len = max_padding_sentence_len
        self.dtype = dtype
        self.dtrain = {
            "word": self.sentences_train,
            "bow": self.bow_train
            }
        self.dtest = {
            "word": self.sentences_test,
            "bow": self.bow_test
            }
        self.ddev = {
            "word": self.sentences_dev,
            "bow": self.bow_dev
            }

    def data(self):
        if self.dtype == "train":
            return self.dtrain
        elif self.dtype == "test":
            return self.dtest
        elif self.dtype == "dev":
            return self.ddev
        else:
            return []

    def train(self):
        self.dtype = "train"; return self
    def test(self):
        self.dtype = "test"; return self
    def dev(self):
        self.dtype = "dev"; return self

    def __len__(self):
        return len(self.data()["word"])

    def __getitem__(self, ix):
        data = self.data()
        # sentVec = []
        p,h,l = data["bow"][ix]
        #_p = self.dictionary.doc2idx(p)
        #_h = self.dictionary.doc2idx(h)
        #_p = list(map(lambda x:x+1, _p))[:self.max_padding_sentence_len]
        #_h = list(map(lambda x:x+1, _h))[:self.max_padding_sentence_len]
        #_p.extend([0] * (self.max_padding_sentence_len - len(_p)))
        #_h.extend([0] * (self.max_padding_sentence_len - len(_h)))
        # sentVec.extend(p)
        # sentVec.extend(h)
        # import pdb; pdb.set_trace()
        return (p, h), l
        # return (p, h), l

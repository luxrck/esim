import gensim
from gensim import corpora, models

import nltk
from nltk.corpus import stopwords
from nltk.tokenize import word_tokenize
from nltk.stem import SnowballStemmer

from tqdm import tqdm

import pandas

import torch
from torch.utils.data import Dataset
import torch.nn.functional as F

from torchvision import transforms

import numpy as np
import pickle
import json
import re

# stemmer = SnowballStemmer("english")
stopwords = stopwords.words("english")
stopwords = []
stopwords.extend(['', '.', ',', '"', "'", ':', ';', '(', ')', '[', ']', '{', '}'])

def sent2words(sent):
    # words = nltk.word_tokenize(sent.lower())
    sent = re.sub(r"[.,'\":;]", "", sent)
    words = set(sent.lower().split())
    words = [w for w in words if w not in stopwords]
    return words

def computeDictionary():
    train = open("snli/snli_1.0_train.jsonl")
    test = open("snli/snli_1.0_test.jsonl")
    dev = open("snli/snli_1.0_dev.jsonl")

    data_train = []
    data_test = []
    data_dev = []
    sentences = []

    max_sentence_len = 0

    def _label(l):
        _t = {
            "entailment": 2,
            "contradiction": 0,
            "neutral": 1
        }
        return _t[l]

    for row in tqdm(train):
        data = json.loads(row)
        if data["gold_label"] == "-": continue
        p = sent2words(data["sentence1"])
        h = sent2words(data["sentence2"])
        try:
            l = _label(data["gold_label"])
        except Exception as e:
            import pdb; pdb.set_trace()
        data_train.append((p,h,l))
        sentences.extend([p, h])

    for row in tqdm(test):
        data = json.loads(row)
        if data["gold_label"] == "-": continue
        p = sent2words(data["sentence1"])
        h = sent2words(data["sentence2"])
        l = _label(data["gold_label"])
        data_test.append((p,h,l))
        sentences.extend([p, h])

    for row in tqdm(dev):
        data = json.loads(row)
        if data["gold_label"] == "-": continue
        p = sent2words(data["sentence1"])
        h = sent2words(data["sentence2"])
        l = _label(data["gold_label"])
        data_dev.append((p,h,l))
        sentences.extend([p, h])

    sent_len = [len(s) for s in sentences]
    max_sentence_len = max(sent_len)
    dictionary = corpora.Dictionary(sentences)
    suggested_padding_sentence_len = int(np.round(np.mean(sent_len) + 4 * np.std(sent_len)))
    max_padding_sentence_len = max_sentence_len
    return data_train, data_test, data_dev, suggested_padding_sentence_len, max_padding_sentence_len, dictionary

sentences_train, sentences_test, sentences_dev, suggested_padding_sentence_len, max_padding_sentence_len, dictionary = computeDictionary()

words = [v[1] for v in dictionary.items()]
# words = pd.read_csv("data/words.csv", sep="\t")
# words = list(words.Word)

gloveVec = {}
gv = open("glove/glove.6B.200d.txt")
for row in tqdm(gv):
    row = row.split()
    gloveVec[row[0]] = [float(n) for n in row[1:]]

wordVec = []
# import pdb; pdb.set_trace()
# 对于idx==0:我定义一个全0的向量.因为之后我要把这个当成nn.Embedding的weight。
wordVec.append(list(np.zeros(200)))
for w in tqdm(words):
    if w in gloveVec:
        wordVec.append(gloveVec[w])
    else:
        print("not in fasttext:", w)
        vec = list(np.random.randn(200))
        gloveVec[w] = vec
        wordVec.append(vec)
# import pdb; pdb.set_trace()


def sent2vec(s):
    return np.sum(np.array([gloveVec[w] for w in s]), axis=0)


#sentences_vec_train = [(sent2vec(s[0]), sent2vec(s[1]), s[2]) for s in sentences_train]
#sentences_vec_test = [(sent2vec(s[0]), sent2vec(s[1]), s[2]) for s in sentences_test]
#sentences_vec_dev = [(sent2vec(s[0]), sent2vec(s[1]), s[2]) for s in sentences_dev]


def sent2bow(s):
    a = dictionary.doc2idx(s)[:suggested_padding_sentence_len]
    a.extend([0] * (suggested_padding_sentence_len - len(s)))
    return a

bow_train = [(sent2bow(s[0]), sent2bow(s[1]), s[2]) for s in tqdm(sentences_train)]
bow_test = [(sent2bow(s[0]), sent2bow(s[1]), s[2]) for s in tqdm(sentences_test)]
bow_dev = [(sent2bow(s[0]), sent2bow(s[1]), s[2]) for s in tqdm(sentences_dev)]


out = {
    # "words": words,
    "word_vec": wordVec,
    "suggested_padding_sentence_len": suggested_padding_sentence_len,
    "max_padding_sentence_len": max_padding_sentence_len,
    "sentences_train": sentences_train,
    "sentences_test": sentences_test,
    "sentences_dev": sentences_dev,
    #"sentences_vec_train": sentences_vec_train,
    #"sentences_vec_test": sentences_vec_test,
    #"sentences_vec_dev": sentences_vec_dev,
    "bow_train": bow_train,
    "bow_test": bow_test,
    "bow_dev": bow_dev,
    "dictionary": dictionary,
    }
print(len(words))
pickle.dump(out, open("data/preprocessing.pkl", "wb+"))